# f6kgl-repo

F6KGL OS package repository

## Add repository to pacman
As F6KGL OS is an Arch based distro, you need to edit the custom repositories in `/etc/pacman.conf`:
```
#/etc/pacman.conf
--------------------------------------------------------
[f6kgl-repo]
SigLevel = Optional TrustAll
Server = https://gitlab.com/f4iey/$repo/-/raw/main/$arch
```
