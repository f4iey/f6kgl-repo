#!/bin/bash
#UPDATEHAM
#shell script to build pkgs and push them into the repo
source ./utils.sh
#updating full repo
git pull
#searching for pkg through yay
# and running makepkg as nobody
echo "Building AUR packages..."
cpt=1
#in presence of arguments, update arguments only
state=1
while [ $# -ne 0 ]
do
	# Before anything, checking if updates are really needed
	if [[ -z $(./check-updates.sh $1 | grep updated) ]]; then
		echo "$1 does not need to be updated!"
	elif [[ -z $(./check-updates.sh $1 | grep AUR) ]]; then
		f6kgl_build $1
		state=0		
	else
		aur_build $1
		state=0
	fi
	shift
	if [ $# -eq 0 ]
	then
		if [ $state -eq 1 ];then exit;fi
		cd x86_64
		refresh_repo
		git add --all
		git commit -m "Update"
		git push -uf origin main
		exit
	fi
done
loop=$(sed -n $cpt'p' aurlist)
while [ ! -z $loop ]
do
	# Before anything, checking if updates are really needed
	if [[ -z $(./check-updates.sh $loop | grep updated) ]]; then
		echo "$loop does not need to be updated!"
	else
		aur_build $loop
	fi
	((cpt=cpt+1))
	loop=$(sed -n $cpt'p' aurlist)
done
# Now searching for PKGs in f6kgl-pkgbuilds
echo "::Building custom core packages from f6kgl-pkgbuilds..."
cpt=1
loop=$(sed -n $cpt'p' pkglist)
while [ ! -z $loop ]
do
	# Before anything, checking if updates are really needed
	if [[ -z $(./check-updates.sh $loop | grep updated) ]]; then
		echo "$loop does not need to be updated!"
	else
		f6kgl_build $loop
	fi
	((cpt=cpt+1))
	loop=$(sed -n $cpt'p' pkglist)
done
echo "Building done!"
#updating db
echo ":: Updating db..."
cd x86_64
refresh_repo
echo "Repo updated locally!"
echo "Ready to push on GitLab..."
git add --all
git commit -m "Update repo db"
# Checking if git remote is configured
# for auto push, make sure git remote is set correctly
# git remote add autopush git+ssh://git@gitlab.com/f4iey/f6kgl-repo.git
git push -uf origin main
