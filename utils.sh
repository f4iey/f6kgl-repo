#!/bin/bash
# Functions
refresh_repo() {
	rm f6kgl-repo*
	repo-add -n -R f6kgl-repo.db.tar.xz *pkg.tar.zst
	rm f6kgl-repo.files f6kgl-repo.db
	mv f6kgl-repo.db.tar.xz f6kgl-repo.db && mv f6kgl-repo.files.tar.xz f6kgl-repo.files
}
aur_build() {
	git clone https://aur.archlinux.org/$1.git
	cd $1
	makepkg -s --noconfirm --rmdeps || return 1
	mv *pkg.tar.zst ../x86_64
	cd .. && rm -rfv $1
}
f6kgl_build() {
	mkdir $1 && cd $1
	curl -fLO https://gitlab.com/f4iey/f6kgl-pkgbuilds/-/raw/main/$1/PKGBUILD
	makepkg -s --noconfirm --rmdeps || return 1
	mv *pkg.tar.zst ../x86_64
	cd .. && rm -rfv $1
}
