#!/bin/bash
#install new pkgs
source ./utils.sh
#we check if the pkg is already in the repo
if [[ -z $(./check-updates.sh $1 | grep repo) ]]; then
    echo "$1 is missing or already installed"
    exit
fi
echo "installing packages..."
state=1
while [ $# -ne 0 ]
do
	# Before anything, checking if updates are really needed
if [[ -z $(./check-updates.sh $1 | grep AUR) ]]; then
		f6kgl_build $1
        echo "marking $1 as installed..."
		echo $1 >> ./pkglist
		state=0
	else
		aur_build $1
		echo "marking $1 as installed..."
		echo $1 >> ./aurlist
		state=0
	fi
	shift
	if [ $# -eq 0 ]
	then
		if [ $state -eq 1 ];then exit;fi
		cd x86_64
		refresh_repo
		git add --all
		git commit -m "Update"
		git push origin main
		exit
	fi
done
