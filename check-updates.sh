# Check for updates before rebuilding
# Checking in the AUR
pkgver=$(curl -sf https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1 | grep -oE 'pkgver=[^=]+$' | cut -c 8-)
pkgrel=$(curl -sf https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1 | grep -oE 'pkgrel=[^=]+$' | cut -c 8-)
pkgtype="AUR"
# If the variables are blank, that means it is not in the AUR
# checking the f6kgl-pkgbuilds
if [[ -z $pkgver || -z $pkgrel ]]; then
	pkgver=$(curl -sf https://gitlab.com/f4iey/f6kgl-pkgbuilds/-/raw/main/$1/PKGBUILD | grep -oE 'pkgver=[^=]+$' | cut -c 8-)
	pkgrel=$(curl -sf https://gitlab.com/f4iey/f6kgl-pkgbuilds/-/raw/main/$1/PKGBUILD | grep -oE 'pkgrel=[^=]+$' | cut -c 8-)
	pkgtype="F6KGL"
fi
# If variables are still null after that, cannot check
if [[ -z $pkgver || -z $pkgrel ]]; then
	echo -e "\033[91mERROR: Build files not found!\033[0m"
	exit
fi
# No args
if [[ $# -eq 0 ]]
then
       exit	
fi
# Compare to the f6kgl repo
# Checking if package exists
aur="$1 $pkgver-$pkgrel"
repo="$(pacman -Sl f6kgl-repo | grep $1 | sed s/f6kgl-repo\ // | sed -n 1p)"
echo "found $pkgtype PKGBUILD: $aur"
if [ "$repo" == "$aur" ]
then
	echo -e "\033[32mPackage $repo is up to date!\033[0m"
elif [[ -z $repo ]]; then
	echo -e "\033[91mERROR: Package $1 not found in the repo\033[0m"
else
	echo -e "\033[91mPackage $repo needs to be updated!\033[0m"
fi
	
